
/ip firewall mangle
add chain=forward src-address=10.1.1.0/24 protocol=tcp dst-port=21 \
action=mark-packet new-packet-mark=semlimite passthrough=yes \
comment="Marcando Pacotes Sem Limite Conexao" disabled=no
add chain=forward src-address=10.1.1.0/24 protocol=tcp dst-port=22 \
action=mark-packet new-packet-mark=semlimite passthrough=yes comment="" \
disabled=no
add chain=forward src-address=10.1.1.0/24 protocol=tcp dst-port=23 \
action=mark-packet new-packet-mark=semlimite passthrough=yes comment="" \
disabled=no
add chain=forward src-address=10.1.1.0/24 protocol=tcp dst-port=25 \
action=mark-packet new-packet-mark=semlimite passthrough=yes comment="" \
disabled=no
add chain=forward src-address=10.1.1.0/24 protocol=tcp dst-port=53 \
action=mark-packet new-packet-mark=semlimite passthrough=yes comment="" \
disabled=no
add chain=forward src-address=10.1.1.0/24 protocol=tcp dst-port=80 \
action=mark-packet new-packet-mark=semlimite passthrough=yes comment="" \
disabled=no
add chain=forward src-address=10.1.1.0/24 protocol=tcp dst-port=110 \
action=mark-packet new-packet-mark=semlimite passthrough=yes comment="" \
disabled=no
add chain=forward src-address=10.1.1.0/24 protocol=tcp dst-port=443 \
action=mark-packet new-packet-mark=semlimite passthrough=yes comment="" \
disabled=no
add chain=forward src-address=10.1.1.0/24 protocol=tcp dst-port=8080 \
action=mark-packet new-packet-mark=semlimite passthrough=yes comment="" \
disabled=no
add chain=forward src-address=10.1.1.0/24 protocol=tcp dst-port=6891-6901 \
action=mark-packet new-packet-mark=semlimite passthrough=yes comment="" \
disabled=no

/ip firewall filter
add chain=forward src-address=10.1.1.0/24 protocol=tcp tcp-flags=syn \
packet-mark=!semlimite connection-limit=25,32 action=drop comment="Limitando \
numero conexoes simultaneas" disabled=no
