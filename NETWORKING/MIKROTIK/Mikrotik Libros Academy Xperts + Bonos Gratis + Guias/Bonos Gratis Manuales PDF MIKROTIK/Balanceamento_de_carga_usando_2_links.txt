/ ip firewall mangle
add chain=prerouting in-interface= Local connection-state=new nth=1,1,0 action=mark-connection new-connection-mark=odd passthrough=yes comment="Balanceamento de carga" disabled=no 

add chain=prerouting in-interface= Local connection-mark=odd action=mark-routing new-routing-mark=odd passthrough=no comment="" disabled=no 

add chain=prerouting in-interface= Local connection-state=new nth=1,1,1 action=mark-connection new-connection-mark=even passthrough=yes comment="" disabled=no 

add chain=prerouting in-interface= Local connection-mark=even action=mark-routing new-routing-mark=even passthrough=no comment="" disabled=no 



/ ip firewall nat 
add chain=srcnat connection-mark=odd action=src-nat to-addresses=192.168.15.199 to-ports=0-65535 comment="Balanceamento de carga" disabled=no 

add chain=srcnat connection-mark=even action=src-nat to-addresses=192.168.241.73 to-ports=0-65535 comment="" disabled=no 



/ ip route 
add dst-address=0.0.0.0/0 gateway=192.168.15.254 scope=255 target-scope=10 routing-mark=odd comment="Gateway SttarNet" disabled=no 

add dst-address=0.0.0.0/0 gateway=192.168.251.254 scope=255 target-scope=10 routing-mark=even comment="Gateway INova" disabled=no 


add dst-address=0.0.0.0/0 gateway=192.168.15.254 scope=255 target-scope=10 comment="principal" disabled=yes
