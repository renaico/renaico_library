add chain=forward p2p=warez time=9h-18h59m,fri,thu,wed,tue,mon action=drop \
    comment="Drop ALL WAREZ-P2P de Lunes a Viernes de 7:00 AM a 11:59 PM, \
    porque Warez no se puede limitar solo drop." disabled=no

add chain=forward src-address=0.0.0.0 dst-address=0.0.0.0 protocol=tcp \
    p2p=all-p2p connection-limit=10,22 action=drop comment="Limita la 
cantidad \
    de conexiones de P2P" disabled=no